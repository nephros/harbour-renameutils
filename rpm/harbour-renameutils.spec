# 
# Do NOT Edit the Auto-generated Part!
# Generated by: spectacle version 0.32
# 

Name:       harbour-renameutils

# >> macros
# << macros
%define upstream_name renameutils

Summary:    The file renaming utilities (renameutils for short) are a set of programs designed to make renaming of files faster and less cumbersome.
Version:    0.12.0
Release:    5
Group:      Applications/System
License:    GNU General Public Licence v3 or any later version
URL:        http://www.nongnu.org/renameutils/
Source0:    https://download.savannah.gnu.org/releases/%{upstream_name}/%{upstream_name}-%{version}.tar.gz
Source100:  harbour-renameutils.yaml
Patch0:     renameutils-0.12.0-typo.patch
Requires:   readline
BuildRequires:  autoconf
BuildRequires:  automake
BuildRequires:  libtool
BuildRequires:  readline-devel
BuildRequires:  rpm
BuildRequires:  gnu-coreutils

%description
The file renaming utilities (renameutils for short) are a set of programs designed to make renaming of files faster and less cumbersome.
The file renaming utilities consists of five programs - qmv, qcp, imv, icp and deurlname.
The qmv ("quick move") program allows file names to be edited in a text editor. The names of all files in a directory are written to a text file, which is then edited by the user. The text file is read and parsed, and the changes are applied to the files.
The qcp ("quick cp") program works like qmv, but copies files instead of moving them.
The imv ("interactive move") program, is trivial but useful when you are too lazy to type (or even complete) the name of the file to rename twice. It allows a file name to be edited in the terminal using the GNU Readline library. icp copies files.
The deurlname program removes URL encoded characters (such as %20 representing space) from file names. Some programs such as w3m tend to keep those characters encoded in saved files.
The file renaming utilities are written in C by Oskar Liljeblad, and is free software licensed under terms of the GNU General Public License.


%package locale
Summary:    Files for %{name}
Group:      Development/Libraries
Requires:   %{name} = %{version}-%{release}

%description locale
Files for %{name}.

%package docs
Summary:    Documentation files for %{name}
Group:      Development/Libraries
Requires:   %{name} = %{version}-%{release}

%description docs
Documentation files for %{name}.

%prep
%setup -q -n %{upstream_name}-%{version}

# renameutils-0.12.0-typo.patch
%patch0 -p1
# >> setup
# << setup

%build
# >> build pre
# use our own (GNU) ls binary per default:
sed -i -e "s#xstrdup(\"ls\")#xstrdup(\"%{_libexecdir}/%{name}/ls\")#" src/qcmd.c
# << build pre

%configure --disable-static \
    --disable-option-checking \
    --disable-dependency-tracking \
    --disable-n

make %{?_smp_mflags}

# >> build post
# << build post

%install
rm -rf %{buildroot}
# >> install pre
# << install pre
%make_install

# >> install post
# use our own (GNU) ls binary per default:
#rm -f /tmp/gnu-coreutils*.rpm ||:
#pkcon download /tmp  gnu-coreutils --filter=~source
#rpm2cpio /tmp/gnu-coreutils*.rpm | cpio -iv --to-stdout ./bin/ls > /tmp/gnu-ls
cp -L /bin/ls /tmp/gnu-ls
%{__install} -c -p -D -m 755 /tmp/gnu-ls $RPM_BUILD_ROOT/%{_libexecdir}/%{name}/ls
# << install post

%files
%defattr(-,root,root,-)
%license COPYING
%{_libexecdir}/%{name}/ls
%{_bindir}/deurlname
%{_bindir}/qcmd
%{_bindir}/icmd
%{_bindir}/qmv
%{_bindir}/qcp
%{_bindir}/imv
%{_bindir}/icp
# >> files
# << files

%files locale
%defattr(-,root,root,-)
# >> files locale
%{_datadir}/locale
# << files locale

%files docs
%defattr(-,root,root,-)
# >> files docs
%{_mandir}
# << files docs
