%define upstream_name renameutils
%define systemddir_user /home/nemo/.config/systemd/user
%define confdir_user /home/nemo/.config/%{name}/%{name}
%define vardir_user /home/nemo/.cache/%{name}/%{name}
Name:		harbour-renameutils
Version:	0.12.0
Release:	1
Summary:	The file renaming utilities (renameutils for short) are a set of programs designed to make renaming of files faster and less cumbersome.
License:    GNU General Public Licence v3 or any later version
URL:		http://www.nongnu.org/renameutils/

Source0:    https://download.savannah.gnu.org/releases/%{upstream_name}/%{upstream_name}-%{version}.tar.gz
Patch0:     renameutils-0.12.0-typo.patch
#Patch1:     m4.patch

BuildRoot: %{_tmppath}/%{upstream_name}-%{version}-%{release}-root

BuildRequires:	autoconf
BuildRequires:	automake
BuildRequires:	libtool
BuildRequires:	readline-devel
Requires:	readline

%description
The file renaming utilities (renameutils for short) are a set of programs designed to make renaming of files faster and less cumbersome.
The file renaming utilities consists of five programs - qmv, qcp, imv, icp and deurlname.
The qmv ("quick move") program allows file names to be edited in a text editor. The names of all files in a directory are written to a text file, which is then edited by the user. The text file is read and parsed, and the changes are applied to the files.
The qcp ("quick cp") program works like qmv, but copies files instead of moving them.
The imv ("interactive move") program, is trivial but useful when you are too lazy to type (or even complete) the name of the file to rename twice. It allows a file name to be edited in the terminal using the GNU Readline library. icp copies files.
The deurlname program removes URL encoded characters (such as %20 representing space) from file names. Some programs such as w3m tend to keep those characters encoded in saved files.
The file renaming utilities are written in C by Oskar Liljeblad, and is free software licensed under terms of the GNU General Public License.

%prep
%autosetup -p1 -n %{upstream_name}-%{version}

%build
# modernize autotools, perhaps it improves the configure stage:
autoreconf -fiv
%configure

gmake %{?_smp_mflags}


%install
make install DESTDIR=$RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%license COPYING
%{_bindir}/deurlname
%{_bindir}/qcmd
%{_bindir}/icmd
%{_bindir}/qmv
%{_bindir}/qcp
%{_bindir}/imv
%{_bindir}/icp
%{_mandir}/man1/*

%post


%preun


%postun


%changelog
* Fri Mar  6 17:31:26 CET 2020 Nephros <sailfish@nephros.org> 0.12.0-1
- initial version of spec file


# this is a non-ASCII character to shut up rpmlint: «
# vim: fileencoding=utf-8

